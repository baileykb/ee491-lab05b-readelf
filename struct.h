///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file struct.h
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   07/03/2021 
///////////////////////////////////////////////////////////////////////////////


typedef struct {

   unsigned char   e_ident[16];     
   uint16_t        e_type;                     
   uint16_t        e_machine;                
   uint32_t        e_version;        
   uint32_t        e_entry;             
   uint32_t        e_phoff;               
   uint32_t        e_shoff;                
   uint32_t        e_flags;              
   uint16_t        e_ehsize;              
   uint16_t        e_phentsize;              
   uint16_t        e_phnum;                
   uint16_t        e_shentsize;                
   uint16_t        e_shnum;                
   uint16_t        e_shstrndx;                

} Elf32_Ehdr;


typedef struct {
   
   unsigned char   e_ident[16];     
   uint16_t        e_type;                     
   uint16_t        e_machine;                
   uint32_t        e_version;        
   uint64_t        e_entry;             
   uint64_t        e_phoff;               
   uint64_t        e_shoff;                
   uint32_t        e_flags;              
   uint16_t        e_ehsize;              
   uint16_t        e_phentsize;              
   uint16_t        e_phnum;                
   uint16_t        e_shentsize;                
   uint16_t        e_shnum;                
   uint16_t        e_shstrndx;                

} Elf64_Ehdr;

typedef struct {
   
   uint32_t   sh_name;
   uint32_t   sh_type;
   uint64_t   sh_flags;
   uint64_t   sh_addr;
   uint64_t   sh_offset;
   uint64_t   sh_size;
   uint32_t   sh_link;
   uint32_t   sh_info;
   uint64_t   sh_addralign;
   uint64_t   sh_entsize;

} Elf64_Shdr;

typedef struct {
   
   uint32_t   sh_name;
   uint32_t   sh_type;
   uint32_t   sh_flags;
   uint32_t   sh_addr;
   uint32_t   sh_offset;
   uint32_t   sh_size;
   uint32_t   sh_link;
   uint32_t   sh_info;
   uint32_t   sh_addralign;
   uint32_t   sh_entsize;

} Elf32_Shdr;
