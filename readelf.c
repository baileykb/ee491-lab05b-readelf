///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file readelf.c
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   23/02/2021
///////////////////////////////////////////////////////////////////////////////

#include "readelf.h"

bool header_opt  = false; 
bool sheader_opt = false; 

int main(int argc, char *argv[]){

   FILE *file;  
   char *filename;
   char c; 
   char buffer[16];
   
   parse(argc, argv);

   for (int i = 1; i < argc; i++){
      if( (argv[i][0] != '-') ){
         filename = argv[i]; 
         file = fopen(filename, "rb"); 
         if ( file == NULL){
            fprintf(stderr, "readelf: File doesn't exist\n");
            exit(EXIT_FAILURE); 
         }

         // Loop through first 16 bytes to load magic section of file into buffer
         for(int i = 0; i < 16; i++){
            c = fgetc(file); 
            buffer[i] = c;
         }
         // Check if the file is an ELF file
            if ( (buffer[0] != 0x7f) || (buffer[1] != 'E') ||  (buffer [2] != 'L') ||(buffer[3] != 'F')){
               fprintf(stderr, "readelf: This is not an ELF file\n"); 
               exit(EXIT_FAILURE);
            }
         
         if(header_opt){
      
            if (buffer[4] == 1){
               Elf32_Ehdr hdr;
               rewind(file);
               fread(&hdr, 1, sizeof(hdr), file);
               fclose(file); 
               print_header32(hdr);
            }
      
            if (buffer[4] == 2){
               Elf64_Ehdr hdr;
               rewind(file);
               fread(&hdr, 1, sizeof(hdr), file); 
               print_header64(hdr); 
            }
         }
         
         if (sheader_opt){
            
            if (buffer[4] == 1){
               Elf32_Ehdr hdr; 
               rewind(file);   
               fread(&hdr, 1, sizeof(hdr), file);
               print_sheader32(hdr, file); 
            } 
            if (buffer[4] == 2){
               Elf64_Ehdr hdr; 
               rewind(file);   
               fread(&hdr, 1, sizeof(hdr), file);
               print_sheader64(hdr, file); 
            } 
         }
      }
   }

   return EXIT_SUCCESS; 

}

void parse(int argc, char  *argv[]){

   int opt = 0;

  //Getopt long struct implemented for future long options
   static struct option long_options[] = {
      {"header",          required_argument, NULL, 'h'},
      {"section-details", required_argument, NULL, 't'},
      {0,                 0,                 0,     0  }
   };

   if ( argc < 3 ){
      fprintf(stderr, "readelf: Invalid number of arguments provided\n");
      exit(EXIT_FAILURE);
   }

  while( (opt = getopt_long(argc, argv, ":ht:", long_options, NULL)) != -1 ) {

     switch(opt){

        case 'h':
           header_opt = true;
           break;
        case 't':
           sheader_opt = true;
           break;
        case '?':
           fprintf(stderr, "readelf: Invalid argument provided\n");
           exit(EXIT_FAILURE);
      }
   }
}

void print_sheader32(Elf32_Ehdr hdr, FILE *file) {

   Elf32_Shdr shdr;

   fprintf(stdout, "Section Headers:\n" ); 
   fprintf(stdout, "  [NR] Name\n " ); 
   fprintf(stdout, "      Type             Addr     Off    Size   ES Lk Inf Al\n" ); 
   fprintf(stdout, "       Flags\n" ); 

   for (int i = 0; i < hdr.e_shnum; i++){
   
      char type[14];
      fseek(file, hdr.e_shoff + (i * hdr.e_shentsize), SEEK_SET); 
      fread(&shdr, 1, sizeof(shdr), file);

      switch(shdr.sh_type){
         case 0: 
            strcpy(type, "NULL"); 
            break; 
         case 1: 
            strcpy(type, "PROGBITS");
            break; 
         case 2:
            strcpy(type, "SYMTAB"); 
            break;
         case 3:
            strcpy(type, "STRTAB"); 
            break; 
         case 4:
            strcpy(type, "RELA"); 
            break; 
         case 5:
            strcpy(type, "HASH"); 
            break; 
         case 6:
            strcpy(type, "DYNAMIC"); 
            break; 
         case 7:
            strcpy(type, "NOTE"); 
            break;
         case 8:
            strcpy(type, "NOBITS"); 
            break; 
         case 9:
            strcpy(type, "REL"); 
            break; 
         case 10:
            strcpy(type, "SHLIB"); 
            break; 
         case 11:
            strcpy(type, "DYNSYM"); 
            break;
         case 14:
            strcpy(type, "INIT_ARRAY");
            break;  
         case 15:
            strcpy(type, "FINI_ARRAY");
            break; 
         case 16:
            strcpy(type, "PREINIT_ARRAY"); 
            break;   
         case 17:
            strcpy(type, "GROUP"); 
            break;   
         case 18:
            strcpy(type, "SYMTAB_SHNDX"); 
            break;   
         case 19:
            strcpy(type, "SHT_NUM"); 
            break;   
         case 0x6ffffffe:
            strcpy(type, "VERDEF"); 
            break; 
         case 0x6ffffff6:
            strcpy(type, "GNU_HASH"); 
            break;  
         case 0x6fffffff:
            strcpy(type, "VERSYM"); 
            break; 
         default: 
            strcpy(type, "N/A"); 
            break; 
            
      }

      fprintf(stdout, "  [%2d]\n", i);  
      fprintf(stdout, "      %-10s        %08x %06x %06x %2x %2d %2d %2x\n",  type,  shdr.sh_addr, shdr.sh_offset, shdr.sh_size, shdr.sh_entsize,shdr.sh_link, shdr.sh_info, shdr.sh_addralign);
      fprintf(stdout, "      [%08x]\n", shdr.sh_flags); 
   }
}
void print_sheader64(Elf64_Ehdr hdr, FILE *file) {

   Elf64_Shdr shdr;

   fprintf(stdout, "Section Headers:\n" ); 
   fprintf(stdout, "  [NR] Name\n " ); 
   fprintf(stdout, "      Type            Address                Offset                Link\n" ); 
   fprintf(stdout, "       Size            EntSize                Info                  Align\n" ); 
   fprintf(stdout, "       Flags\n" ); 
   
   for (int i = 0; i < hdr.e_shnum; i++){
   
      char type[14];
      fseek(file, hdr.e_shoff + (i * hdr.e_shentsize), SEEK_SET); 
      fread(&shdr, 1, sizeof(shdr), file);

      switch(shdr.sh_type){
         case 0: 
            strcpy(type, "NULL"); 
            break; 
         case 1: 
            strcpy(type, "PROGBITS");
            break; 
         case 2:
            strcpy(type, "SYMTAB"); 
            break;
         case 3:
            strcpy(type, "STRTAB"); 
            break; 
         case 4:
            strcpy(type, "RELA"); 
            break; 
         case 5:
            strcpy(type, "HASH"); 
            break; 
         case 6:
            strcpy(type, "DYNAMIC"); 
            break; 
         case 7:
            strcpy(type, "NOTE"); 
            break;
         case 8:
            strcpy(type, "NOBITS"); 
            break; 
         case 9:
            strcpy(type, "REL"); 
            break; 
         case 10:
            strcpy(type, "SHLIB"); 
            break; 
         case 11:
            strcpy(type, "DYNSYM"); 
            break;
         case 14:
            strcpy(type, "INIT_ARRAY");
            break;  
         case 15:
            strcpy(type, "FINI_ARRAY");
            break; 
         case 16:
            strcpy(type, "PREINIT_ARRAY"); 
            break;   
         case 17:
            strcpy(type, "GROUP"); 
            break;   
         case 18:
            strcpy(type, "SYMTAB_SHNDX"); 
            break;   
         case 19:
            strcpy(type, "SHT_NUM"); 
            break;   
         case 0x6ffffffe:
            strcpy(type, "VERDEF"); 
            break; 
         case 0x6ffffff6:
            strcpy(type, "GNU_HASH"); 
            break;  
         case 0x6fffffff:
            strcpy(type, "VERSYM"); 
            break; 
         default: 
            strcpy(type, "N/A"); 
            break; 
            
      }

      fprintf(stdout, "  [%2d]\n", i);  
      fprintf(stdout, "      %-16s  %016lx %016lx %016x\n",  type,  shdr.sh_addr, shdr.sh_offset, shdr.sh_link);
      fprintf(stdout, "      %016lx  %016lx %-16d %-16lx\n", shdr.sh_size, shdr.sh_entsize, shdr.sh_info, shdr.sh_addralign); 
      fprintf(stdout, "      [%016lx]\n", shdr.sh_flags); 
   }
}

// Note: Referred to man elf 5 for assistance
void print_header32(Elf32_Ehdr hdr){
  
   fprintf(stdout, "ELF Header:\n"); 
   fprintf(stdout, "  Magic: "); 
   for (int i = 0; i <16; i++){
      fprintf(stdout, "%02x ", hdr.e_ident[i]); 
   }

   fprintf(stdout, "\n  Class:                             ELF32\n"); 
  
   switch(hdr.e_ident[5]){
      case 1: 
         fprintf(stdout,"  Data:                              2's complement, little endian\n"); 
         break; 
      case 2:
         fprintf(stdout,"  Data:                              2's complement, big endian\n");
         break;

   }

   switch(hdr.e_ident[6]){
      case 1: 
         fprintf(stdout, "  Version:                           1 (current)\n");
         break; 
      case 0:
         fprintf(stdout,"   Version:                           Invalid version\n"); 
   }

   switch(hdr.e_ident[7]){

      case 0:
         fprintf(stdout, "  OS/ABI:                            UNIX System V\n");
         break; 
      
      case 1:
         fprintf(stdout, "  OS/ABI:                            HP-UX\n");
         break; 

      case 2:
         fprintf(stdout, "  OS/ABI:                            1 (current)\n");
         break; 

   }

   fprintf(stdout, "  ABI Version:                       %d\n", hdr.e_ident[8]);

   switch(hdr.e_type){
      case 1:
         fprintf(stdout, "  Type:                              REL (Relocatable file)\n");
         break;
      case 2:
         fprintf(stdout, "  Type:                              EXEC (Executable file)\n");
         break;
      case 3:
         fprintf(stdout, "  Type:                              DYN (Shared object)\n");
         break;
      case 4:
         fprintf(stdout, "  Type:                              CORE (Core file)\n");
         break;
   }

   switch(hdr.e_machine){
      case 1:
         fprintf(stdout, "  Machine:                           AT&T WE 32100\n"); 
         break; 
       case 62:
         fprintf(stdout, "  Machine:                           Advanced Micro Devices x86-64\n"); 
         break; 


   }

   switch(hdr.e_version){
      case 0:
         fprintf(stdout, "  Version:                           Invalid Version\n");
      case 1:
         fprintf(stdout, "  Version:                           0x%x\n", hdr.e_version);

   }

  fprintf(stdout, "  Entry point address:               0x%x\n", hdr.e_entry);  
  fprintf(stdout, "  Start of program headers:          %d (bytes into file)\n", hdr.e_phoff );
  fprintf(stdout, "  Start of section headers:          %d (bytes into file)\n", hdr.e_shoff);  
  fprintf(stdout, "  Flags:                             0x%x \n", hdr.e_flags);
  fprintf(stdout, "  Size of this header:               %d (bytes)\n", hdr.e_ehsize); 
  fprintf(stdout, "  Size of program headers:           %d (bytes)\n", hdr.e_phentsize);
  fprintf(stdout, "  Number of program headers:         %d\n", hdr.e_phnum);
  fprintf(stdout, "  Size of section headers:           %d (bytes)\n", hdr.e_shentsize);
  fprintf(stdout, "  Number of section headers:         %d \n", hdr.e_shnum); 
  fprintf(stdout, "  Section header string table index: %d\n", hdr.e_shstrndx); 

}

void print_header64(Elf64_Ehdr hdr){
   
   fprintf(stdout, "ELF Header:\n"); 
   fprintf(stdout, "  Magic: "); 
   for (int i = 0; i <16; i++){
      fprintf(stdout, "%02x ", hdr.e_ident[i]); 
   }

   fprintf(stdout, "\n  Class:                             ELF64\n"); 
  
   switch(hdr.e_ident[5]){
      case 1: 
         fprintf(stdout,"  Data:                              2's complement, little endian\n"); 
         break; 
      case 2:
         fprintf(stdout,"  Data:                              2's complement, big endian\n");
         break;

   }

   switch(hdr.e_ident[6]){
      case 1: 
         fprintf(stdout, "  Version:                           1 (current)\n");
         break; 
      case 0:
         fprintf(stdout,"   Version:                           Invalid version\n"); 
   }

   switch(hdr.e_ident[7]){

      case 0:
         fprintf(stdout, "  OS/ABI:                            UNIX System V\n");
         break; 
      
      case 1:
         fprintf(stdout, "  OS/ABI:                            HP-UX\n");
         break; 

      case 2:
         fprintf(stdout, "  OS/ABI:                            1 (current)\n");
         break; 

   }

   fprintf(stdout, "  ABI Version:                       %d\n", hdr.e_ident[8]);

   switch(hdr.e_type){
      case 1:
         fprintf(stdout, "  Type:                              REL (Relocatable file)\n");
         break;
      case 2:
         fprintf(stdout, "  Type:                              EXEC (Executable file)\n");
         break;
      case 3:
         fprintf(stdout, "  Type:                              DYN (Shared object)\n");
         break;
      case 4:
         fprintf(stdout, "  Type:                              CORE (Core file)\n");
         break;
   }

   switch(hdr.e_machine){
      case 1:
         fprintf(stdout, "  Machine:                           AT&T WE 32100\n"); 
         break; 
       case 62:
         fprintf(stdout, "  Machine:                           Advanced Micro Devices x86-64\n"); 
         break; 


   }

   switch(hdr.e_version){
      case 0:
         fprintf(stdout, "  Version:                           Invalid Version\n");
      case 1:
         fprintf(stdout, "  Version:                           0x%x\n", hdr.e_version);

   }

  fprintf(stdout, "  Entry point address:               0x%lx\n", hdr.e_entry);  
  fprintf(stdout, "  Start of program headers:          %ld (bytes into file)\n", hdr.e_phoff );
  fprintf(stdout, "  Start of section headers:          %ld (bytes into file)\n", hdr.e_shoff);  
  fprintf(stdout, "  Flags:                             0x%x \n", hdr.e_flags);
  fprintf(stdout, "  Size of this header:               %d (bytes)\n", hdr.e_ehsize); 
  fprintf(stdout, "  Size of program headers:           %d (bytes)\n", hdr.e_phentsize);
  fprintf(stdout, "  Number of program headers:         %d\n", hdr.e_phnum);
  fprintf(stdout, "  Size of section headers:           %d (bytes)\n", hdr.e_shentsize);
  fprintf(stdout, "  Number of section headers:         %d \n", hdr.e_shnum); 
  fprintf(stdout, "  Section header string table index: %d\n", hdr.e_shstrndx); 

}
