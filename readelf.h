///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file readelf.h
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   23/02/2021
///////////////////////////////////////////////////////////////////////////////
#ifndef READELF_H
#define READELF_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include "struct.h"
#include <string.h>

void print_header32(Elf32_Ehdr hdr);
void print_header64(Elf64_Ehdr hdr);
void parse(int argc, char *argv[]);
void print_sheader64(Elf64_Ehdr, FILE *file);
void print_sheader32(Elf32_Ehdr, FILE *file);

extern bool header_opt;
extern bool sheader_opt; 

#endif
