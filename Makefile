CC     = gcc
CFLAGS = -g -Wall

TARGET = readelf

all: $(TARGET)

readelf: readelf.c
	$(CC) $(CFLAGS) -o $(TARGET) readelf.c 

test: 
	./readelf -h ./testfiles/test1
	./readelf -h ./testfiles/test2
	./readelf -h ./testfiles/test3
	./readelf -h ./testfiles/test4
	./readelf -h ./testfiles/test5
	./readelf -h ./testfiles/test6
	./readelf -h ./testfiles/test7
clean:
	rm $(TARGET)

